﻿using MusicOverFlow.DataLayer.Entities.Song;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Album;

namespace MusicOverFlow.Core.Convertors
{
    public static class ToViewModelConvertor
    {
        public static Task<List<ShowListSongViewModel>> ToAsyncList(List<Song> songs, IAlbumService albumService, ISingerService singerService)
        {
            var songList = songs.Select(s => new ShowListSongViewModel()
            {
                AlbumId = s.AlbumId,
                AlbumName = albumService.GetNameById(s.AlbumId),
                Id = s.Id,
                Image = s.Image,
                ModifyDate = s.ModifyDate,
                Name = s.Name,
                SingerId = s.SingerId,
                SongFile = s.SongFile,
                SingerName = singerService.GetNameById(s.SingerId)
            });


            return Task.Run(() => songList.ToList());

        }

        public static Task<List<ShowListAlbumViewModel>> ToAsyncList(List<Album> albums, ISingerService singerService)
        {
            var albumList = albums.Select(a => new ShowListAlbumViewModel()
            {
                SingerId = a.SingerId,
                Name = a.Name,
                Image = a.Image,
                Id = a.Id,
                ModifyDate = a.ModifyDate,
                SingerName = singerService.GetNameById(a.SingerId)
            });

            return Task.Run(() => albumList.ToList());
        }
    }
}
