﻿using System;
using MongoDB.Driver;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Context;
using MusicOverFlow.DataLayer.Entities.Song;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MusicOverFlow.Core.Services
{
    public class SongService : ISongService
    {
        private readonly IMongoCollection<Song> _songs;

        public SongService(IMongoDbSetting setting)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("MusicOverFlow");

            _songs = database.GetCollection<Song>("Song");
        }

        public string GetNameById(string id) => Get(id).Name;

        public void Create(Song song) => _songs.InsertOne(song);

        public bool Exists(string id) => _songs.Find(s => s.Id == id).Any();

        public bool Exists(string songName, string singerId) => _songs.Find(s => s.SingerId == singerId && s.Name == songName).Any();

        public Song Get(string id) => _songs.Find(s => s.Id == id).FirstOrDefault();

        public List<Song> GetSongsByAlbumId(string id) => _songs.Find(s => s.AlbumId == id).ToList();

        public List<Song> Get() => _songs.Find(Song => true).ToList();

        public Task<List<Song>> GetAsync(Expression<Func<Song,bool>> filter,FindOptions options = null) =>
            _songs.Find(filter).ToListAsync();

        public List<Song> GetSongsBySingerId(string id) => _songs.Find<Song>(s => s.SingerId == id).ToList();

        public void Remove(string id) => _songs.DeleteOne(s => s.Id == id);

        public void Update(string id, Song song) => _songs.ReplaceOne(s => s.Id == id, song);

        public long CountByAlbumId(string id)
        {
            return _songs.CountDocuments(s => s.AlbumId == id);
        }

        public long Count()
        {
            return _songs.CountDocuments(Song => true);
        }
    }
}
