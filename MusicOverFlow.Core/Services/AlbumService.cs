﻿using MongoDB.Driver;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Context;
using MusicOverFlow.DataLayer.Entities.Album;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MusicOverFlow.Core.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly IMongoCollection<Album> _albums;

        public AlbumService(IMongoDbSetting setting)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("MusicOverFlow");

            _albums = database.GetCollection<Album>("Album");
        }

        public bool Exists(string id) => _albums.Find(a => a.Id == id).Any();

        public bool ExistsByName(string name) => _albums.Find(a => a.Name == name).Any();

        public Album Get(string id) => _albums.Find(s => s.Id == id).FirstOrDefault();

        public string GetNameById(string id)
        {
            if (string.IsNullOrEmpty(id))
                return "-";

            if (!Exists(id))
                return "-";
            return _albums.Find(a => a.Id == id).SingleOrDefault().Name;
        }

        public List<Album> Get() => _albums.Find(Album => true).ToList();
        public Task<List<Album>> GetAsync(Expression<Func<Album, bool>> filter, FindOptions options = null) => _albums.Find(filter).ToListAsync();

        public void Create(Album album) => _albums.InsertOne(album);

        public void Delete(string id) => _albums.DeleteOne(a => a.Id == id);

        public List<Album> GetSingerAlbums(string singerId) => _albums.Find(a => a.SingerId == singerId).ToList();

        public long Count()=>  _albums.CountDocuments(Album => true);

        public void Update(string id, Album album)
        {
            _albums.ReplaceOne(a => a.Id == id, album);
        }
    }
}
