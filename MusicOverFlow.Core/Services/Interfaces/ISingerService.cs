﻿using MusicOverFlow.DataLayer.Entities.Singer;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MusicOverFlow.Core.Services.Interfaces
{
    public interface ISingerService
    {
        bool Exists(string id);
        bool ExistsByName(string name);
        Singer Get(string id);
        List<Singer> Get();
        Task<List<Singer>> GetAsync(Expression<Func<Singer, bool>> filter, FindOptions options = null);
        string GetNameById(string id);
        Singer Create(Singer singer);
        void Update(string id, Singer singer);
        void Remove(string id);
        long Count();
    }
}
