﻿using MusicOverFlow.DataLayer.Entities.Song;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MusicOverFlow.Core.Services.Interfaces
{
    public interface ISongService
    {
        bool Exists(string id);
        bool Exists(string songName, string singerId);
        Song Get(string id);
        List<Song> GetSongsBySingerId(string id);
        List<Song> GetSongsByAlbumId(string id);
        List<Song> Get();
        Task<List<Song>> GetAsync(Expression<Func<Song, bool>> filter, FindOptions options = null);
        string GetNameById(string id);
        void Create(Song song);
        void Update(string id, Song song);
        void Remove(string id);
        long CountByAlbumId(string id);
        long Count();

    }
}
