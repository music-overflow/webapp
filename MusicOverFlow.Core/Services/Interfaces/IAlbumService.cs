﻿using MusicOverFlow.DataLayer.Entities.Album;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Core.Services.Interfaces
{
    public interface IAlbumService
    {
        bool Exists(string id);
        bool ExistsByName(string name);
        Album Get(string id);
        string GetNameById(string id);
        List<Album> Get();
        Task<List<Album>> GetAsync(Expression<Func<Album, bool>> filter, FindOptions options = null);
        void Create(Album album);
        void Delete(string id);
        void Update(string id, Album album);
        List<Album> GetSingerAlbums(string singerId);
        long Count();
    }
}
