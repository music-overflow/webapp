﻿using MongoDB.Driver;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Context;
using MusicOverFlow.DataLayer.Entities.Singer;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MusicOverFlow.Core.Services
{
    public class SingerService : ISingerService
    {

        private readonly IMongoCollection<Singer> _singers;

        public SingerService(IMongoDbSetting setting)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("MusicOverFlow");

            _singers = database.GetCollection<Singer>("Singer");
        }

        public string GetNameById(string id) => Get(id).FullName;

        public Singer Create(Singer singer)
        {
            _singers.InsertOne(singer);
            return singer;
        }

        public void Update(string id,Singer singer)
        {
            _singers.ReplaceOne(s => s.Id == id, singer);
        }

        public bool Exists(string id) => _singers.Find(s => s.Id == id).Any();
        public bool ExistsByName(string name) => _singers.Find(s => s.FullName == name).Any();
        public Singer Get(string id) => _singers.Find(s => s.Id == id).FirstOrDefault();
        public List<Singer> Get() => _singers.Find(Singer => true).ToList();
        public Task<List<Singer>> GetAsync(Expression<Func<Singer, bool>> filter, FindOptions options = null) => _singers.Find(filter).ToListAsync();

        public void Remove(string id)=>_singers.DeleteOne(s=> s.Id == id);
        public long Count() => _singers.CountDocuments(Singer => true);
    }
}
