﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class CreateAlbumViewModel
    {
        [Required(ErrorMessage = "لطفا {0} را وارد کنید.")]
        [Display(Name = "نام")]
        public string Name { get; set; }

        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید.")]
        [Display(Name = "خواننده")]
        public string SingerId { get; set; }

        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید.")]
        [Display(Name = "عکس")]
        public IFormFile ImageFile { get; set; }

    }
}
