﻿using System;
using System.Collections.Generic;
using System.Text;
using MusicOverFlow.DataLayer.Entities.Album;
using MusicOverFlow.DataLayer.Entities.Singer;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Core.DTOs
{
    public class AlbumProfileViewModel
    {
        public Album Album { get; set; }
        public Singer Singer { get; set; }
        public IEnumerable<Song> Songs { get; set; }
    }
}
