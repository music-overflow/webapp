﻿using MusicOverFlow.DataLayer.Entities.Singer;
using MusicOverFlow.DataLayer.Entities.Song;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class ShowSingerProfileViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Image { get; set; }

        public string Bio { get; set; }
        
        public DateTime ModifyDate { get; set; }

        public IEnumerable<ShowListSongViewModel> Songs { get; set; }
        public IEnumerable<ShowListAlbumViewModel> Albums { get; set; }
        public IEnumerable<Singer> Singers { get; set; }
    }
}
