﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class EditSingerViewModel
    {

        public string Id { get; set; }
        [Required(ErrorMessage = "لطفا {0} را وارد کنید.")]
        [Display(Name = "نام خواننده")]
        public string FullName { get; set; }

        [Display(Name = "عکس خواننده")]
        public IFormFile ProfileImage { get; set; }

        public string ImageString { get; set; }

        [Required(ErrorMessage = "لطفا {0} را وارد کنید.")]
        [Display(Name = "درباره خواننده")]
        public string Bio { get; set; }
    }
}
