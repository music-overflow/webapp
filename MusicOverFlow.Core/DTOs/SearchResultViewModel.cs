﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MusicOverFlow.DataLayer.Entities.Singer;

namespace MusicOverFlow.Core.DTOs
{
    public class SearchResultViewModel
    {
        public List<ShowListSongViewModel> Songs { get; set; }
        public List<Singer> Singers { get; set; }
        public List<ShowListAlbumViewModel> Albums { get; set; }
        public string SearchText { get; set; }
    }
}
