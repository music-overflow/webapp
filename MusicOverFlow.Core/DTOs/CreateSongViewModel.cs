﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class CreateSongViewModel
    {

        [Display(Name = "نام")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Name { get; set; }

        [Display(Name = "عکس")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید")]
        public IFormFile Image { get; set; }

        [Display(Name = "فایل اهنگ")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید")]
        public IFormFile SongFile { get; set; }

        [Display(Name = "خواننده")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید")]
        public string SingerId { get; set; }

        [Display(Name = "البوم")]
        public string AlbumId { get; set; }
        public bool IsSingleTrack { get; set; }

    }
}
