﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class SearchIndexViewModel
    {
        public long SingerCount { get; set; }
        public long SongCount { get; set; }
        public long AlbumCount { get; set; }
    }
}
