﻿using System;
using System.Collections.Generic;
using System.Text;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Core.DTOs
{
    public class SongPlayerViewModel
    {
        public ShowListSongViewModel PlayingSong { get; set; }
        public IEnumerable<ShowListSongViewModel> Songs  { get; set; }
    }
}
