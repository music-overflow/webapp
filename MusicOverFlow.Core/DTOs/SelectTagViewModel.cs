﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class SelectTagViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
