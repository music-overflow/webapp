﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.Core.DTOs
{
    public class ShowListSongViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string SingerId { get; set; }
        public string SingerName { get; set; }
        public string AlbumId { get; set; }
        public string AlbumName { get; set; }
        public string SongFile { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
