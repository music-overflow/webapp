using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MusicOverFlow.Core.Services;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Context;

namespace MusicOverFlow.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            #region DB Context

            services.Configure<MongoDbSetting>(
       Configuration.GetSection(nameof(MongoDbSetting)));

            services.AddSingleton<IMongoDbSetting>(sp =>
                sp.GetRequiredService<IOptions<MongoDbSetting>>().Value);

            #endregion

            #region IoC

            services.AddTransient<IAlbumService, AlbumService>();
            services.AddTransient<ISingerService, SingerService>();
            services.AddTransient<ISongService, SongService>();

            #endregion

            //This will handle requests up to 700MB - baes mishe betoonam ta 700MB file ro yekja upload konam
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = 5000;
                x.MultipartBodyLengthLimit = 737280000;
                x.MultipartHeadersLengthLimit = 737280000;

            });
            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = 837280000;
            });
                       
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAreaControllerRoute(
                    name: "areas",
                    areaName: "admin",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=home}/{action=index}/{id?}");
            });

        }
    }
}
