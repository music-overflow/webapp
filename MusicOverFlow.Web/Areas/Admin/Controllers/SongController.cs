﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Generator;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class SongController : Controller
    {
        private readonly ISongService _songService;
        private readonly ISingerService _singerService;
        private readonly IAlbumService _albumService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SongController(ISongService songService, ISingerService singerService, IAlbumService albumService, IWebHostEnvironment webHostEnvironment)
        {
            _songService = songService;
            _singerService = singerService;
            _albumService = albumService;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {

            var songList = _songService.Get().Select(s => new ShowListSongViewModel()
            {
                Name = s.Name,
                AlbumId = s.AlbumId,
                AlbumName = _albumService.GetNameById(s.AlbumId),
                SingerId = s.SingerId,
                SingerName = _singerService.GetNameById(s.SingerId),
                Id = s.Id,
                Image = s.Image,
                ModifyDate = s.ModifyDate
            });

            return View(songList);
        }

        [HttpGet]
        public IActionResult Create()
        {
            #region set singers and albums in ViewData

            ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
            {
                Id = s.Id,
                Name = s.FullName
            });

            #endregion

            return View();
        }

        [HttpPost]
        public IActionResult Create(CreateSongViewModel song)
        {
            if (!ModelState.IsValid)
            {

                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });


                return View(song);
            }
            if (_songService.Exists(song.Name, song.SingerId))
            {
                
                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });


                ModelState.AddModelError("All", $"خواننده مورد نظر در حال حاضر یک اهنگ با نام {song.Name} دارد");
                return View(song);
            }

            try
            {
                _songService.Create(new Song()
                {
                    Name = song.Name,
                    SingerId = song.SingerId,
                    AlbumId = (song.IsSingleTrack)? null: song.AlbumId,
                    ModifyDate = DateTime.Now,
                    Image = UploadSongImage(song.Image),
                    SongFile = UploadSongFile(song.SongFile)
                });
            }
            catch (Exception)
            {
                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });

                ModelState.AddModelError("All", "در ثبت اهنگ مشکلی پیش امده، لطفا دوباره تلاش کنید.");
                return View(song);
            }



            return RedirectToAction("Index");
        }

        public IActionResult Delete(string id)
        {
            if (_songService.Exists(id))
            {
                var song = _songService.Get(id);
                _songService.Remove(id);

                DeleteSongFile(song.SongFile);
                DeleteSongImage(song.Image);

                return RedirectToAction("Index");

            }

            return NotFound();
        }

        #region Upload Functions

        private string UploadSongImage(IFormFile file)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "Song");
                uniqueFileName = NameGenerator.GenerateUniqCode() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
        private string UploadSongFile(IFormFile file)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "Audio", "Song");
                uniqueFileName = NameGenerator.GenerateUniqCode() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        #endregion

        #region Delete Functions

        private void DeleteSongImage(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                string img = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "Album", fileName);
                if (System.IO.File.Exists(img))
                {
                    System.IO.File.Delete(img);
                }
            }
        }
        private void DeleteSongFile(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                string img = Path.Combine(_webHostEnvironment.WebRootPath, "Audio", "Song", fileName);
                if (System.IO.File.Exists(img))
                {
                    System.IO.File.Delete(img);
                }
            }
        }

        #endregion

    }
}
