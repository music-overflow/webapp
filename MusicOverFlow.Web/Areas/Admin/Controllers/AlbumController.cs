﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Differencing;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Generator;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Album;
using MusicOverFlow.DataLayer.Entities.Singer;

namespace MusicOverFlow.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AlbumController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly IAlbumService _albumService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AlbumController(ISingerService singerService, IAlbumService albumService, IWebHostEnvironment webHostEnvironment)
        {
            _singerService = singerService;
            _albumService = albumService;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
            var albumList = _albumService.Get().Select(a => new ShowListAlbumViewModel()
            {
                SingerId = a.SingerId,
                Id = a.Id,
                ModifyDate = a.ModifyDate,
                Name = a.Name,
                SingerName = _singerService.Get(a.SingerId).FullName,
                Image = a.Image
            });

            return View(albumList);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
            {
                Id = s.Id,
                Name = s.FullName
            });

            return View();
        }

        [HttpPost]
        public IActionResult Create(CreateAlbumViewModel album)
        {

            if (!ModelState.IsValid)
            {
                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });
                return View(album);
            }
            //if exists don't add
            if (_albumService.ExistsByName(album.Name))
            {
                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });
                ModelState.AddModelError("All", $"یک البوم با نام {album.Name} در حال حاضر وجود دارد");
                return View(album);
            }

            try
            {
                _albumService.Create(new Album()
                {
                    Image = UploadAlbumImage(album.ImageFile),
                    ModifyDate = DateTime.Now,
                    Name = album.Name,
                    SingerId = album.SingerId
                });
            }
            catch (Exception)
            {
                ModelState.AddModelError("All", "در ثبت البوم مشکلی پیش امده، لطفا دوباره تلاش کنید.");
                ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                {
                    Id = s.Id,
                    Name = s.FullName
                });
                return View(album);
            }

            return RedirectToAction("Index", "Album", new { area = "Admin" });
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (_albumService.Exists(id))
                {
                    ViewData["singer"] = _singerService.Get().Select(s => new SelectTagViewModel()
                    {
                        Id = s.Id,
                        Name = s.FullName
                    });
                    var album = _albumService.Get(id);
                    EditAlbumViewModel vm = new EditAlbumViewModel()
                    {
                        ImageString = album.Image,
                        Name = album.Name,
                        SingerId = album.SingerId,
                        Id = album.Id
                    };
                    return View(vm);
                }
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Edit(EditAlbumViewModel album)
        {
            if (string.IsNullOrEmpty(album.Id))
            {
                return RedirectToAction("Index", "Album", new { area = "Admin" });
            }
            if (ModelState.IsValid)
            {
                if (_albumService.Exists(album.Id))
                {
                    var OldAlbum = _albumService.Get(album.Id);
                    var uniqName = OldAlbum.Image;

                    if (album.ImageFile != null)
                    {
                        DeleteAlbumImage(OldAlbum.Image);
                        uniqName = UploadAlbumImage(album.ImageFile);
                    }

                    Album newAlbum = new Album()
                    {
                        Id = album.Id,
                        Image = uniqName,
                        ModifyDate = OldAlbum.ModifyDate,
                        Name = album.Name,
                        SingerId = album.SingerId
                    };
                    _albumService.Update(album.Id, newAlbum);
                    return RedirectToAction("Index", "Album", new { area = "Admin" });

                }
            }

            return View(album);
        }

        public IActionResult Delete(string id)
        {
            string img = _albumService.Get(id).Image;
            //Check if the album has any songs
            _albumService.Delete(id);
            DeleteAlbumImage(img);

            return RedirectToAction("Index", "Album", new { area = "Admin" });
        }

        private string UploadAlbumImage(IFormFile file)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "Album");
                uniqueFileName = NameGenerator.GenerateUniqCode() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
        private void DeleteAlbumImage(string fileName)
        {
            string img = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "Album", fileName);
            if (System.IO.File.Exists(img))
            {
                System.IO.File.Delete(img);
            }
        }
    }
}