﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.Core.DTOs;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using MusicOverFlow.Core.Generator;
using MusicOverFlow.DataLayer.Entities.Singer;

namespace MusicOverFlow.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SingerController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly IWebHostEnvironment _hostEnvironment;

        public SingerController(ISingerService singerService, IWebHostEnvironment webHostEnvironment)
        {
            _singerService = singerService;
            _hostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult Index() => View(_singerService.Get());

        [HttpGet]
        public IActionResult Create() => View();

        [HttpPost]
        public IActionResult Create(CreateSingerViewModel singer)
        {
            if (!ModelState.IsValid)
            {
                return View(singer);
            }
            if (_singerService.ExistsByName(singer.FullName))
            {
                ModelState.AddModelError("All", "خواننده ای با همین نام وجود دارد");
                return View(singer);
            }

            string uniqueFileName = UploadSingerImage(singer.ProfileImage);

            uniqueFileName = (uniqueFileName != null) ? uniqueFileName : "Default.jpg";

            _singerService.Create(new DataLayer.Entities.Singer.Singer()
            {
                Bio = singer.Bio,
                FullName = singer.FullName,
                Image = uniqueFileName,
                ModifyDate = DateTime.Now
            });

            return RedirectToAction("index");
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (_singerService.Exists(id))
                {
                    Singer singer = _singerService.Get(id);
                    EditSingerViewModel vm = new EditSingerViewModel()
                    {
                        ImageString = singer.Image,
                        FullName = singer.FullName,
                        Id = singer.Id,
                        Bio = singer.Bio
                    };
                    return View(vm);
                }
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Edit(EditSingerViewModel singer)
        {
            if (string.IsNullOrEmpty(singer.Id))
            {
                ModelState.AddModelError("All", "مشکلی پیش آمده است، لطفا دوباره تلاش کنید.");
            }
            if (ModelState.IsValid)
            {
                if (_singerService.Exists(singer.Id))
                {

                    var OldSinger = _singerService.Get(singer.Id);
                    var uniqName = OldSinger.Image;

                    if (singer.ProfileImage != null)
                    {
                        DeleteSingerImage(OldSinger.Image);
                        uniqName = UploadSingerImage(singer.ProfileImage);
                    }

                    Singer newSinger = new Singer()
                    {
                        Id = singer.Id,
                        Bio = singer.Bio,
                        FullName = singer.FullName,
                        Image = uniqName,
                        ModifyDate = OldSinger.ModifyDate
                    };
                    _singerService.Update(singer.Id, newSinger);
                    return RedirectToAction("Index");

                }
            }
            return View(singer);

        }

        public IActionResult Delete(string id)
        {
            string img = _singerService.Get(id).Image;
            _singerService.Remove(id);
            DeleteSingerImage(img);

            return RedirectToAction("Index");
        }

        private string UploadSingerImage(IFormFile file)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_hostEnvironment.WebRootPath, "Images", "Singer");
                uniqueFileName = NameGenerator.GenerateUniqCode() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
        private void DeleteSingerImage(string fileName)
        {
            string img = Path.Combine(_hostEnvironment.WebRootPath, "Images", "Singer", fileName);
            if (System.IO.File.Exists(img))
            {
                System.IO.File.Delete(img);
            }
        }
    }
}
