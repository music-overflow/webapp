﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Controllers
{
    public class AlbumController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;

        public AlbumController(ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        [Route("Albums")]
        public IActionResult Index()
        {
            var albums = _albumService.Get().Select(a => new ShowListAlbumViewModel() {
                Id = a.Id,
                Image = a.Image,
                ModifyDate = a.ModifyDate,
                Name = a.Name,
                SingerId = a.SingerId,
                SingerName = _singerService.GetNameById(a.SingerId),
                SongCount = _songService.CountByAlbumId(a.Id)
            }).ToList();

            return View(albums);
        }

        [Route("Album/{id?}")]
        public IActionResult Profile(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (_albumService.Exists(id))
                {
                    AlbumProfileViewModel vm = new AlbumProfileViewModel();
                    vm.Album = _albumService.Get(id);
                    vm.Singer = _singerService.Get(vm.Album.SingerId);
                    vm.Songs = _songService.GetSongsByAlbumId(id);

                    return View(vm);
                }
            }
            return NotFound();
        }
    }
}
