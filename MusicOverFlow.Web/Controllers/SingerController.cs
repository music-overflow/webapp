﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Singer;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Controllers
{
    public class SingerController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;
        public SingerController(ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        [Route("Singer/{id?}")]
        public IActionResult Profile(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Singers");
            }
            Singer singer = _singerService.Get(id);
            if (singer == null)
            {
                return NotFound();
            }
            IEnumerable<Singer> singers = _singerService.Get().Take(4);

            var songList = _songService.GetSongsBySingerId(id).Select(s => new ShowListSongViewModel()
            {
                Name = s.Name,
                AlbumId = s.AlbumId,
                AlbumName = _albumService.GetNameById(s.AlbumId),
                SingerId = s.SingerId,
                SingerName = _singerService.GetNameById(s.SingerId),
                Id = s.Id,
                Image = s.Image,
                ModifyDate = s.ModifyDate
            });

            var albumList = _albumService.GetSingerAlbums(id).Select(a => new ShowListAlbumViewModel()
            {
                Id = a.Id,
                Image = a.Image,
                ModifyDate = a.ModifyDate,
                Name = a.Name,
                SingerId = a.Name,
                SingerName = singer.FullName,
                SongCount = _songService.CountByAlbumId(a.Id)
            });

            ShowSingerProfileViewModel singerProfile = new ShowSingerProfileViewModel()
            {
                FullName = singer.FullName,
                Bio = singer.Bio,
                Image = singer.Image,
                Id = singer.Id,
                ModifyDate = singer.ModifyDate,
                Singers = singers,
                Songs = songList,
                Albums = albumList
            };


            return View(singerProfile);
        }
        [Route("Singers")]
        public IActionResult Singers()
        {
            return View(_singerService.Get());
        }
    }
}
