﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.Web.Models;

namespace MusicOverFlow.Web.Controllers
{
    public class HomeController : Controller
    {
	
        private readonly ILogger<HomeController> _logger;
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;

        public HomeController(ILogger<HomeController> logger, ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _logger = logger;
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        public IActionResult Index()
        {
            ViewData["Singers"] = _singerService.Get().OrderByDescending(s=>s.ModifyDate).Take(4).ToList();
            ViewData["TopAlbum"] = _albumService.Get().OrderByDescending(s => s.ModifyDate).Take(2).ToList();
            ViewData["Albums"] = _albumService.Get().OrderByDescending(s => s.ModifyDate).Take(4).ToList();

            ViewData["NewSingleTracks"] = _songService.Get()
                .Where(s => string.IsNullOrEmpty(s.AlbumId))
                .OrderByDescending(s => s.ModifyDate).Take(4).ToList();

            return View();
        }

        [Route("ContactUs")]
        public IActionResult ContactUs() => View();

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}