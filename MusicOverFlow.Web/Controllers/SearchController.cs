﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicOverFlow.Core.Convertors;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Controllers
{
    public class SearchController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;

        public SearchController(ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        [Route("/search")]
        [HttpGet]
        public async Task<IActionResult> Search([FromQuery(Name = "search")] string search)
        {

            if (!(string.IsNullOrEmpty(search) || string.IsNullOrWhiteSpace(search)))
            {
                SearchResultViewModel result = new SearchResultViewModel();

                var songs = _songService.GetAsync(s =>
                    s.Name.ToLower() == search.ToLower() || s.Name.ToLower().Contains(search.ToLower()));
                var albums = _albumService.GetAsync(a =>
                    a.Name.ToLower() == search || a.Name.ToLower().Contains(search.ToLower()));
                var singers = _singerService.GetAsync(s =>
                    s.FullName.ToLower() == search || s.FullName.ToLower().Contains(search.ToLower()));

                result.Songs = await ToViewModelConvertor.ToAsyncList(songs.Result, _albumService, _singerService);
                result.Albums = await ToViewModelConvertor.ToAsyncList(albums.Result, _singerService);
                result.Singers = await singers;
                result.SearchText = search;
                return View("Search", result);
            }
            else
            {
                SearchIndexViewModel vm = new SearchIndexViewModel()
                {
                    SingerCount = _singerService.Count(),
                    SongCount = _songService.Count(),
                    AlbumCount = _albumService.Count()
                };

                return View("Index",vm);
            }

        }
    }
}