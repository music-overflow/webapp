﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;

namespace MusicOverFlow.Web.Controllers
{
    public class SongController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;

        public SongController(ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        [Route("Songs")]
        public IActionResult Index()
        {
            var vm = _songService.Get().Select(s => new ShowListSongViewModel()
            {
                AlbumId = s.AlbumId,
                AlbumName = _albumService.GetNameById(s.AlbumId),
                Id = s.Id,
                Image = s.Image,
                ModifyDate =s.ModifyDate,
                Name =s.Name,
                SingerId=s.SingerId,
                SingerName = _singerService.GetNameById(s.SingerId),
                SongFile = s.SongFile
            }).ToList();
            return View(vm);
        }
    }
}
