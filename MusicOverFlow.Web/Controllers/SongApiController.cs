﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongApiController : ControllerBase
    {
        private readonly ISingerService _singerService;
        private readonly IAlbumService _albumService;
        private readonly ISongService _songService;

        public SongApiController(ISingerService singerService, IAlbumService albumService, ISongService songService)
        {
            _singerService = singerService;
            _albumService = albumService;
            _songService = songService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ShowListSongViewModel>> Get()
        {
            return _songService.Get().Select(s => new ShowListSongViewModel()
            {
                Name = s.Name,
                AlbumId = s.AlbumId,
                AlbumName = _albumService.GetNameById(s.AlbumId),
                SingerId = s.SingerId,
                SingerName = _singerService.GetNameById(s.SingerId),
                Id = s.Id,
                Image = s.Image,
                ModifyDate = s.ModifyDate,
                SongFile = s.SongFile
            }).ToList();
        }

        [HttpGet("{id:length(24)}", Name = "Get")]
        public ActionResult<ShowListSongViewModel> Get(string id)
        {
            var song = _songService.Get(id);

            return new ShowListSongViewModel()
            {
                Name = song.Name,
                AlbumId = song.AlbumId,
                AlbumName = _albumService.GetNameById(song.AlbumId),
                SingerId = song.SingerId,
                SingerName = _singerService.GetNameById(song.SingerId),
                Id = song.Id,
                Image = song.Image,
                ModifyDate = song.ModifyDate,
                SongFile = song.SongFile
            };
        }
    }
}
