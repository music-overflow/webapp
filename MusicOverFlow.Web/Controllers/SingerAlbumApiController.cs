﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Album;

namespace MusicOverFlow.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SingerAlbumApiController : ControllerBase
    {
        private readonly ISingerService _singerService;
        private readonly IAlbumService _albumService;

        public SingerAlbumApiController(ISingerService singerService, IAlbumService albumService)
        {
            _singerService = singerService;
            _albumService = albumService;
        }

        [HttpGet("{id:length(24)}")]
        public ActionResult<List<Album>> Get(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (_singerService.Exists(id))
                {
                    return _albumService.GetSingerAlbums(id);
                }
            }
            return NotFound();
        }
    }
}
