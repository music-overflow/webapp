﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.DTOs;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Song;

namespace MusicOverFlow.Web.Controllers
{
    public class PlayerController : Controller
    {
        private readonly ISingerService _singerService;
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;
        public PlayerController(ISingerService singerService, ISongService songService, IAlbumService albumService)
        {
            _singerService = singerService;
            _songService = songService;
            _albumService = albumService;
        }

        [Route("Play/{id?}")]
        public IActionResult Play(string id)
        {
            var play = new SongPlayerViewModel();
            var selectedSong = _songService.Get(id);
            play.PlayingSong = new ShowListSongViewModel()
            {
                Name =  selectedSong.Name,
                AlbumId = selectedSong.AlbumId,
                AlbumName = _albumService.GetNameById(selectedSong.AlbumId),
                Id = selectedSong.Id,
                SingerId = selectedSong.SingerId,
                Image = selectedSong.Image,
                ModifyDate = selectedSong.ModifyDate,
                SingerName = _singerService.GetNameById(selectedSong.SingerId),
                SongFile = selectedSong.SongFile
            };

            List<Song> selectedSongs = new List<Song>();

            //if the song wasn't in a album then give 10 random songs from database
            if (!_albumService.Exists(play.PlayingSong.AlbumId))
            {
                var songs = _songService.Get().Take(20);
                List<int> nums = new List<int>();
                var rand = new Random();
                do
                {
                    int selectedNum = rand.Next(songs.Count());
                    if (nums.All(n => n != selectedNum))
                    {
                        nums.Add(selectedNum);
                    }
                } while (nums.Count < 9);

                foreach (int i in nums)
                {
                    selectedSongs.Add(songs.ToList()[i]);
                }

                if (selectedSongs.Any(s=> s.Id != id))
                {
                    selectedSongs.Add(_songService.Get(id));
                }
            }
            else
            {
                //makes playing an album passible
                selectedSongs = _songService.GetSongsByAlbumId(play.PlayingSong.AlbumId).Take(10).ToList();
            }

            play.Songs = selectedSongs.Select(ss=> new ShowListSongViewModel()
            {
                AlbumId = ss.AlbumId,
                SingerId = ss.SingerId,
                Name = ss.Name,
                Image = ss.Image,
                AlbumName = _albumService.GetNameById(ss.AlbumId),
                Id = ss.Id,
                ModifyDate = ss.ModifyDate,
                SingerName = _singerService.GetNameById(ss.SingerId)
            });
            return View(play);
        }
    }
}
