﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MusicOverFlow.Core.Services.Interfaces;
using MusicOverFlow.DataLayer.Entities.Album;

namespace MusicOverFlow.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlbumApiController : ControllerBase
    {
        private readonly ISingerService _singerService;
        private readonly IAlbumService _albumService;

        public AlbumApiController(ISingerService singerService, IAlbumService albumService)
        {
            _singerService = singerService;
            _albumService = albumService;
        }
        
        [HttpGet]
        public ActionResult<List<Album>> Get() => _albumService.Get();

        [HttpGet("{id:length(24)}", Name = "GetAlbum")]
        public ActionResult<Album> Get(string id) => _albumService.Get(id);

    }
}
