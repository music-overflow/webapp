﻿var repeat = false,
    shuffle = false;
var music = document.getElementById("playingMusic");
var music_time = $("#time-range");
var playingMusicId;

function make00(Number) {

    intNumber = parseInt(Number);
    if (intNumber === 0) {
        return "00";
    }

    var _00Number = "";
    if (intNumber <= 9) {
        _00Number = ('0' + intNumber.toString());
        return _00Number;
    }
    _00Number = intNumber.toString();
    return _00Number;
}

function timeListener(event) {
    $("#time-range").attr("value", parseInt(event.currentTime));
    document.getElementById('time-left').innerHTML = make00((event.currentTime / 60)) + ':' + make00((event
        .currentTime % 60));
}

function PlayPause(state) {
    if (state === "") {
        if (music.paused) {
            music.play();
            $("#btn_playpause").removeClass("zmdi-play").addClass("zmdi-pause");
            document.getElementById('whole-time').innerHTML = make00((music.duration / 60)) + ':' + make00((music
                .duration % 60));
            $("#time-range").attr("max", parseInt(music.duration));
            $("#time-range").attr("min", 000);

        } else {
            music.pause();
            $("#btn_playpause").removeClass("zmdi-pause").addClass("zmdi-play");

        }
    } else {
        if (state === "pause") {
            if (!music.paused) {
                music.pause();
                $("#btn_playpause").removeClass("zmdi-pause").addClass("zmdi-play");
            }
        }
        if (state === "play") {
            if (!music.played) {
                music.play();
                $("#btn_playpause").removeClass("zmdi-play").addClass("zmdi-pause");
                document.getElementById('whole-time').innerHTML = make00((music.duration / 60)) + ':' + make00((music
                    .duration % 60));
                $("#time-range").attr("max", parseInt(music.duration));
                $("#time-range").attr("min", 000);
            }
        }
    }
};

function musicEnded() {
    $("#btn_playpause").removeClass("zmdi-pause").addClass("zmdi-play");
}

$('.playing-music-image').click(function () {
    PlayPause("");
});
$('#btn_playpause').click(function () {

    PlayPause("");
});

music_time.on("input", function () {
    var musicTimeVal = $(this).val();
    music.currentTime = musicTimeVal;
    document.getElementById('time-left').innerHTML = make00((musicTimeVal / 60)) + ':' + make00((
        musicTimeVal % 60));
});

$(".volume-value").on("input", function () {
    music.volume = ($(this).val() / 100);
});

$(".Change_Music").click(function (e) {
    e.preventDefault();

    if ($(this).attr('id') !== playingMusicId) {

        PlaySong($(this).attr('id'));
    }
});

$(".zmdi-skip-next").on("click",
    function () {
        NextSong();
    });
$(".zmdi-skip-previous").on("click",
    function () {
        var theId;
        var tbodyTag = $(".tbl-songs table tbody");
        var firstSongId = tbodyTag.find(":first-child td:first").text();
        //Get the last child in the list bebin ke aakhariye ya na, age bood az avali dobare shuru kon
        if (playingMusicId !== firstSongId.toString()) {
            theId = $(".playing-song").parents("tr:first").prev().find(".song-id").text();

            PlaySong(theId);
        }
    });
$(".btn-shuffle").on("click", function () {
    if (shuffle === true) {
        shuffle = false;
        $(".btn-shuffle").removeClass('bg-dark').removeClass("text-white");

    } else {
        shuffle = true;
        $(".btn-shuffle").addClass('bg-dark').addClass("text-white");
    }

});
$(".btn-repeat").on("click", function () {
    if (repeat === true) {
        repeat = false;
        $(".btn-repeat").removeClass('bg-dark').removeClass("text-white");

    } else {
        repeat = true;
        $(".btn-repeat").addClass('bg-dark').addClass("text-white");

    }
});

$(".volume-bar").mouseover(function () {
    $("#volume-holder").fadeIn(1000);
});
$("#volume-holder").mouseleave(function () {

    $(this).fadeOut(1000);
});

function ChangeBackground(background) {
    $(".playing-background").fadeOut(600,
        function () {
            $(this).css("background-image", "url(/Images/Song/" + background + ")");
        }).fadeIn();
}

//ye alamete => kenare ahangi ke dare play mishe mizare
function SignPlayingSong(songId) {
    $(".song-id:contains(" + songId + ")").next().find(".song-name")
        .html(function () {
            return " <span class=\"playing-song zmdi zmdi-play\"></span> " + $(this).text();
        });
}
function RemovePlayingSign() {
    $(".playing-song").remove();
}

function NextSong() {
    var theId;
    var tbodyTag = $(".tbl-songs table tbody");
    var lastSongId = tbodyTag.find(":last-child td:first").text();

    //Get the last child in the list bebin ke aakhariye ya na, age bood az avali dobare shuru kon
    if (playingMusicId === lastSongId.toString()) {
        //agar aakharin ahang ro play karde
        //TODO: check kon inam dorost kar kone
        if (repeat) {
            theId = tbodyTag.find(":first-child .song-id").text();
            PlaySong(theId);

        } else {
            PlayPause("pause");
        }
    }
    else {
        theId = $(".playing-song").parents("tr:first").next().find(".song-id").text();
        PlaySong(theId);
    }

    //$(".playing-song").parent().parent().parent().next("td .song-id:contains(" + playingMusicId + ")");
}

function PlaySong(songId) {
    var url = "/api/SongApi/" + songId;
    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {


            document.title = data.name + "Music OverFlow";
            window.history.pushState(data, data.name + " - Music OverFlow", "/Play/" + data.id);
            PlayPause("pause");

            //bayad meghdare playing song id ro ham avaz koni
            playingMusicId = songId;

            //Change everything of the playing song (and the signs)
            $(".playing-song-name").fadeOut(800,
                function () {
                    $(this).text(data.name);
                }).fadeIn();
            $(".playing-singer-name").fadeOut(850,
                function () {
                    $(this).text(data.singerName);
                }).fadeIn();
            $(".playing-music-image").fadeOut(500,
                function () {
                    $(this).attr("src", "/Images/Song/" + data.image);
                }).fadeIn();
            ChangeBackground(data.image.toString());


            RemovePlayingSign();
            SignPlayingSong(data.id);

            //Setting time and duration stuff
            music_time.val(0);
            music_time.attr("max", music.duration);
            music.currentTime = 0;
            $("#time-left").text("00:00");
            $("#whole-time").html(make00((music.duration / 60)) +
                ':' +
                make00((music
                    .duration %
                    60)));

            $("#playingMusic").attr("src", "/Audio/Song/" + data.songFile);


            PlayPause("play");

        },
        error: function (errorThrown) {
            alert(errorThrown);
            alert("There is an error with AJAX!");
        }
    });
}