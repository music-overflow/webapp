﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.DataLayer.Context
{

    public interface IMongoDbSetting
    {
        string CollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }


    public class MongoDbSetting : IMongoDbSetting
    {
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        }
}
