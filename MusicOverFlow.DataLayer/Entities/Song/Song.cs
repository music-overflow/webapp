﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MusicOverFlow.DataLayer.Entities.Singer;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.DataLayer.Entities.Song
{
    public class Song
    {
        public Song()
        {

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name"), BsonRequired]
        public string Name { get; set; }

        [BsonElement("Image"), BsonRequired]
        public string Image { get; set; }

        [BsonElement("Song"), BsonRequired]
        public string SongFile { get; set; }

        [BsonElement("SingerId"), BsonRequired]
        public string SingerId { get; set; }

        [BsonElement("Album")]
        public string AlbumId { get; set; }

        [BsonRequired]
        public DateTime ModifyDate { get; set; }


        #region Navigation Property

        //public virtual MusicOverFlow.DataLayer.Entities.Singer.Singer Singer { get; set; }

        #endregion
    }
}