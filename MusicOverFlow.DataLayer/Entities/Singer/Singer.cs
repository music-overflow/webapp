﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.DataLayer.Entities.Singer
{
    public class Singer
    {
        public Singer()
        {

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FullName"), BsonRequired]
        public string FullName { get; set; }

        [BsonElement("Image"), BsonRequired]
        public string Image { get; set; }

        [BsonElement("Bio"), BsonRequired]
        public string Bio { get; set; }
        [BsonRequired]
        public DateTime ModifyDate { get; set; }

    }
}
