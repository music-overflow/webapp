﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicOverFlow.DataLayer.Entities.Album
{
    public class Album
    {
        public Album()
        {

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name"), BsonRequired]
        public string Name { get; set; }

        [BsonElement("SingerId"), BsonRequired]
        public string SingerId { get; set; }

        [BsonElement("Image"), /*BsonRequired*/]
        public string Image { get; set; }

        [BsonRequired]
        public DateTime ModifyDate { get; set; }



        #region Navigation Property

        //public virtual MusicOverFlow.DataLayer.Entities.Singer.Singer Singer { get; set; }
        //public virtual ICollection<MusicOverFlow.DataLayer.Entities.Song.Song> Songs { get; set; }

        #endregion

    }
}
